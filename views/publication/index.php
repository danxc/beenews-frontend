<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notícias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Notícia', ['create'], ['class' => 'btn btn-primary solid blank']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'content',
            'pub_date',
            array(
                'label'=>'Categoria',
                'value'=>'category.name',
            ),



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
