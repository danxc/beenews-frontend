<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Publication */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publication-form" style="">

    <?php $form = ActiveForm::begin(); ?>
        <div class="error-container"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                <?=
                    $form->field($model, 'category_id')->dropDownList(
                    ArrayHelper::map(\app\models\Category::find()->all(),'id','name'),
                        ['prompt'=>'Selecione a categoria'])
                ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= $form->field($model, 'content')->textarea([ 'rows' => 6]) ?>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'pub_date')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-right">
                <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-primary solid blank' : 'btn btn-primary']) ?>
            </div>
        </div>




    <?php ActiveForm::end(); ?>

<div style="clear:both; height: 20px;"></div>
