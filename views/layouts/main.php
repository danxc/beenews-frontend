<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

$formatter = \Yii::$app->formatter;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
$last_news = \app\models\Publication::find()->orderBy(['id'=> SORT_DESC])->limit(3)->all();

?>

<div class="body-inner">

    <!-- Trending start -->
    <div class="trending-bar hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="trending-title">NOTÍCIAS</h3>
                    <div id="trending-slide" class="owl-carousel owl-theme trending-slide">

                        <?php foreach ($last_news as $publication){ ?>
                            <!-- Item  start -->
                            <div class="item">
                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="<?=Url::to(['publication/view', 'id'=>$publication->id])?>"><?=$publication->title?></a>
                                    </h2>
                                </div><!-- Post content end -->
                            </div>
                            <!-- Item  end -->
                        <?php } ?>

                    </div><!-- Carousel end -->
                </div><!-- Col end -->
            </div><!--/ Row end -->
        </div><!--/ Container end -->
    </div>
    <!--/ Trending end -->

    <!-- Topbar start -->
    <div id="top-bar" class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="ts-date">
                        <i class="fa fa-calendar-check-o"></i>Novembro 01, 2017
                    </div>
                    <ul class="unstyled top-nav">
                        <li><a href="#">Sobre</a></li>
                        <li><a href="#">Escreva para Beenews</a></li>
                        <li><a href="#">Anunciar</a></li>
                        <li><a href="#">Contato</a></li>
                    </ul>
                </div><!--/ Top bar left end -->

                <div class="col-md-4 col-sm-4 col-xs-12 top-social text-right">
                    <ul class="unstyled">
                        <li>
                            <a title="Facebook" href="#">
                                <span class="social-icon"><i class="fa fa-facebook"></i></span>
                            </a>
                            <a title="Twitter" href="#">
                                <span class="social-icon"><i class="fa fa-twitter"></i></span>
                            </a>
                            <a title="Google+" href="#">
                                <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                            </a>
                            <a title="Linkdin" href="#">
                                <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                            </a>
                        </li>
                    </ul><!-- Ul end -->
                </div><!--/ Top social col end -->
            </div><!--/ Content row end -->
        </div><!--/ Container end -->
    </div>
    <!--/ Topbar end -->

    <!-- Header start -->
    <header id="header" class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="logo">
                        <a href="index.html">
                            <img src="<?=Yii::getAlias('@web')?>/images/logo.jpg" alt="">
                        </a>
                    </div>
                </div><!-- logo col end -->

                <div class="col-xs-12 col-sm-9 col-md-9 header-right">
                    <div class="ad-banner pull-right">
                        <a href="#"><img src="images/banner-ads/ad-top-header.png" class="img-responsive" alt=""></a>
                    </div>
                </div><!-- header right end -->
            </div><!-- Row end -->
        </div><!-- Logo and banner area end -->
    </header>
    <!--/ Header end -->

    <!-- Menu wrapper start -->
    <div class="main-nav dark-bg clearfix">
        <div class="container">
            <div class="row">
                <nav class="site-navigation navigation">
                    <div class="site-nav-inner pull-left">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">

                                <li>
                                    <a href="<?=Url::to('/')?>">Home</a>
                                </li>

                                <li>
                                    <a href="<?=Url::to(['/publication'])?>">Lista Notícias (Temporário)</a>
                                </li>

                                <li>
                                    <a href="<?=Url::to(['/category'])?>">Lista Categorias (Temporário)</a>
                                </li>




                            </ul><!--/ Nav ul end -->
                        </div><!--/ Collapse end -->

                    </div><!-- Site Navbar inner end -->
                </nav><!--/ Navigation end -->

                <div class="nav-search">
                    <span id="search"><i class="fa fa-search"></i></span>
                </div><!-- Search end -->

                <div class="search-block" style="display: none;">
                    <input type="text" class="form-control" placeholder="Type what you want and enter">
                    <span class="search-close">&times;</span>
                </div><!-- Site search end -->

            </div><!--/ Row end -->
        </div><!--/ Container end -->

    </div>
    <!-- Menu wrapper end -->

    <?= $content ?>
    <!-- Footer start -->
    <footer id="footer" class="footer">
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 footer-widget">
                        <h3 class="widget-title">Trending Now</h3>
                        <div class="list-post-block">
                            <ul class="list-post">
                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-responsive" src="images/news/lifestyle/health2.jpg" alt="" />
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="#">Can't shed those Gym? The problem might...</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">Mar 13, 2017</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 1 end -->

                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-responsive" src="images/news/lifestyle/health3.jpg" alt="" />
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="#">Deleting fears from the brain means you…</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">Jan 11, 2017</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 2 end -->

                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                                <img class="img-responsive" src="images/news/lifestyle/health4.jpg" alt="" />
                                            </a>
                                        </div><!-- Post thumb end -->

                                        <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="#">Smart packs parking sensor tech...</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">Feb 19, 2017</span>
                                            </div>
                                        </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 3 end -->
                            </ul><!-- List post end -->
                        </div><!-- List post block end -->

                    </div><!-- Col end -->

                    <div class="col-md-3 col-sm-12 footer-widget widget-categories">
                        <h3 class="widget-title">Hot Categories</h3>
                        <ul>
                            <li>
                                <a href="#"><span class="catTitle">Robotics</span><span class="catCounter"> (5)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Games</span><span class="catCounter"> (6)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Gadgets</span><span class="catCounter"> (5)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Travel</span><span class="catCounter"> (5)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Health</span><span class="catCounter"> (5)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Architecture</span><span class="catCounter"> (5)</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="catTitle">Food</span><span class="catCounter"> (5)</span></a>
                            </li>
                        </ul>

                    </div><!-- Col end -->

                    <div class="col-md-3 col-sm-12 footer-widget twitter-widget">
                        <h3 class="widget-title">Latest Tweets</h3>
                        <ul>
                            <li>
                                <div class="tweet-text">
                                    <span>About 13 days ago</span>
                                    Please, Wait for the next version of our templates to update #Joomla 3.7
                                    <a href="#">https://t.co/LlEv8HgokN</a>
                                </div>
                            </li>
                            <li>
                                <div class="tweet-text">
                                    <span>About 15 days ago</span>
                                    #WordPress 4.8 is here!
                                    <a href="#">https://t.co/uDjRH4Gya9</a>
                                </div>
                            </li>
                            <li>
                                <div class="tweet-text">
                                    <span>About 1 month ago</span>
                                    Please, Wait for the next version of our templates to update #Joomla 3.7
                                    <a href="#">https://t.co/LlEv8HgokN</a>
                                </div>
                            </li>
                        </ul>
                    </div><!-- Col end -->

                    <div class="col-md-3 col-sm-12 footer-widget">
                        <h3 class="widget-title">Post Gallery</h3>
                        <div class="gallery-widget">
                            <a href="#"><img class="img-responsive" src="images/news/lifestyle/health1.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/lifestyle/food3.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/lifestyle/travel4.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/lifestyle/architecture1.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/tech/gadget1.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/tech/gadget2.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/tech/game2.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/tech/robot5.jpg" alt="" /></a>
                            <a href="#"><img class="img-responsive" src="images/news/lifestyle/travel5.jpg" alt="" /></a>
                        </div>
                    </div><!-- Col end -->

                </div><!-- Row end -->
            </div><!-- Container end -->
        </div><!-- Footer main end -->


    </footer>
    <!-- Footer end -->

   



</div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
