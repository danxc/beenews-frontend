<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="error-container"></div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?=
                $form->field($model, 'color')->dropDownList(
                    [
                            'color-aqua'=>'Aqua',
                            'color-blue'=>'Azul',
                            'color-orange'=>'Laranja',
                            'color-red'=>'Vermelho',
                            'color-violet'=>'Violeta',
                    ],
                    ['prompt'=>'Selecione a cor'])
                ?>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-primary solid blank' : 'btn btn-primary']) ?>
    </div>
</div>




<?php ActiveForm::end(); ?>


</div>
