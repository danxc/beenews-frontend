<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Bee news - Site de notícias';
$slider_publications = \app\models\Publication::find()->orderBy(['id'=> SORT_DESC])->limit(3)->all();
$side_publications = $slider_publications;
?>
<!-- Trending post start -->
<section class="featured-post-area no-padding">
    <div class="container">
        <div class="row">
            <!-- Main Publications -->
            <div class="col-md-7 col-xs-12 pad-r">
                <div id="featured-slider" class="owl-carousel owl-theme featured-slider">
                    <?php foreach ($slider_publications as $publication){ ?>
                        <div class="item" style="background-image:url(images/news/lifestyle/health5.jpg)">
                            <div class="featured-post">
                                <div class="post-content">
                                    <a class="post-cat" href="#"><?=$publication->category->name?></a>
                                    <h2 class="post-title title-extra-large">
                                        <a href="<?=Url::to(['publication/view', 'id'=>$publication->id])?>"><?=$publication->title?></a>
                                    </h2>
                                    <span class="post-date"></span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div><!-- Featured owl carousel end-->
            </div><!-- Col 7 end -->
            <!-- Main Publications End -->

            <!-- Others Publications -->
            <div class="col-md-5 col-xs-12 pad-l">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="post-overaly-style contentTop hot-post-top clearfix">
                            <div class="post-thumb">
                                <a href=""><img class="img-responsive" src="images/news/tech/gadget4.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#"><?= $side_publications[0]->category->name?></a>
                                <h2 class="post-title title-large">
                                    <a href="<?=Url::to(['publication/view', 'id'=>$side_publications[0]->id])?>"><?= $side_publications[0]->title?></a>
                                </h2>
                                <span class="post-date"><?= $side_publications[0]->pub_date ?></span>
                            </div><!-- Post content end -->
                        </div><!-- Post Overaly end -->
                    </div><!-- Col end -->

                    <div class="col-sm-6 pad-r-small">
                        <div class="post-overaly-style contentTop hot-post-bottom clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-responsive" src="images/news/lifestyle/travel2.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#"><?=$side_publications[1]->category->name?></a>
                                <h2 class="post-title title-medium">
                                    <a href="<?=Url::to(['publication/view', 'id'=>$side_publications[1]->id])?>"><?=$side_publications[1]->title?></a>
                                </h2>
                            </div><!-- Post content end -->
                        </div><!-- Post Overaly end -->
                    </div><!-- Col end -->

                    <div class="col-sm-6 pad-l-small">
                        <div class="post-overaly-style contentTop hot-post-bottom clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-responsive" src="images/news/lifestyle/health1.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#"><?=$side_publications[2]->category->name?></a>
                                <h2 class="post-title title-medium">
                                    <a href="<?=Url::to(['publication/view', 'id'=>$side_publications[2]->id])?>"><?=$side_publications[2]->title?></a>
                                </h2>
                            </div><!-- Post content end -->
                        </div><!-- Post Overaly end -->
                    </div><!-- Col end -->
                </div>
            </div><!-- Col 5 end -->
            <!-- Others Publications End -->


        </div><!-- Row end -->
    </div><!-- Container end -->
</section>
<!-- Trending post end -->

<?php
    $categories = \app\models\Category::find()->limit(3)->all();

?>
<!-- 2nd block starts -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">

            <?php foreach ($categories as $category){?>
            <div class="col-md-4">
                <div class="block <?= $category->color ?>">
                    <h3 class="block-title"><span><?= $category->name ?></span></h3>
                    <div class="post-overaly-style clearfix">
                        <div class="post-thumb">
                            <a href="#">
                                <img class="img-responsive" src="images/news/lifestyle/travel1.jpg" alt="" />
                            </a>
                        </div>

                        <div class="post-content">
                            <h2 class="post-title">
                                <a href="<?=Url::to(['/publications/view', 'id'=>$category->publications[0]->id])?>"><?= $category->publications[0]->title?></a>
                            </h2>
                            <div class="post-meta">
                                <span class="post-date"><?= $category->publications[0]->pub_date ?></span>
                            </div>
                        </div><!-- Post content end -->
                    </div><!-- Post Overaly Article end -->

                    <div class="list-post-block">
                        <ul class="list-post">
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                            <img class="img-responsive" src="images/news/lifestyle/travel2.jpg" alt="" />
                                        </a>
                                    </div><!-- Post thumb end -->

                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Early tourists choices to the sea of Maldives in fancy dress…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Mar 13, 2017</span>
                                        </div>
                                    </div><!-- Post content end -->
                                </div><!-- Post block style end -->
                            </li><!-- Li 1 end -->

                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                            <img class="img-responsive" src="images/news/lifestyle/travel3.jpg" alt="" />
                                        </a>
                                    </div><!-- Post thumb end -->

                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">This Aeroplane that looks like a butt is the largest aircraf…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Jan 11, 2017</span>
                                        </div>
                                    </div><!-- Post content end -->
                                </div><!-- Post block style end -->
                            </li><!-- Li 2 end -->

                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                            <img class="img-responsive" src="images/news/lifestyle/travel4.jpg" alt="" />
                                        </a>
                                    </div><!-- Post thumb end -->

                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">19 incredible photos from Disney's 'Star Wars' cruise algore</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Feb 19, 2017</span>
                                        </div>
                                    </div><!-- Post content end -->
                                </div><!-- Post block style end -->
                            </li><!-- Li 3 end -->
                        </ul><!-- List post end -->
                    </div><!-- List post block end -->
                </div><!-- Block end -->
            </div><!-- Travel Col end -->
            <?php } ?>


        </div><!-- Row end -->
    </div><!-- Container end -->
</section>
<!-- 2nd block end -->

